\documentclass[11pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subcaption}
\usepackage{caption}
\usepackage{placeins}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{Chittarajan Srinivas and Weiwei Feng}
\title{Lab Assignment 2 - Asus Xtion Sensor}
\begin{document}
\maketitle
\section*{Task 1 and 2}

In this task, a few snapshots are presented. Figure
\ref{fig:rgb_image} shows the image available from
``/camera/rgb/image\_raw''. This topic publishes the raw image from the
sensor. A rectified image is published on
``/camera/rgb/image\_rect\_color'' once calibration is performed and the
parameters are loaded.\\

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.50]{rgb_image.png}
    \caption{Image from /camera/rgb/image\_raw.}
    \label{fig:rgb_image}
\end{figure}

Figure \ref{fig:depth_image} shows the scaled depth image from topic
``/camera/depth\_registered/image\_raw''. This topic publishes the raw
depth data from the sensor. Each pixel contains the depth data for
that pixel. This message is used by depth\_image\_proc to generate
point clouds. \\

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.50]{depth_image.jpg}
    \caption{Image from /camera/depth\_registered/image\_raw.}
    \label{fig:depth_image}
\end{figure}

Figure \ref{fig:point_cloud} shows a snapshot of pcl\_viewer
displaying the point cloud from
``/camera/depth\_registered/points''. These points also contain color
information since they come from the registered point cloud. On another
topic, ``/camera/depth/points'', unregistered point clouds are
published.\\

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{points.jpg}
    \caption{Point Clouds in pcl\_viewer. This is the same scene as
      figure \ref{fig:rgb_image}.}
    \label{fig:point_cloud}
\end{figure}

Figure \ref{fig:point_cloud_registered} shows a snapshot from rviz of
the registered point\_cloud.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.40]{points_registered.jpg}
    \caption{Point cloud from /camera/depth\_registered/points. This
      is the same scene as figure \ref{fig:depth_image} and \ref{fig:target_scene}.}
    \label{fig:point_cloud_registered}
\end{figure}

\section*{Task 3}

In this task, the rgb camera was calibrated. This creates a yaml file
that is loaded every time the \textit{openni2.launch} file is
launched. Once this is loaded, the launch file publishes the
rectification (stereo cameras), projection and camera matrices and the
distortion parameters on the ``/camera/rgb/camera\_info'' topic. The
rectified images are also available, on the
``/camera/rgb/image\_rect\_color'' topic.\\

\textbf{Q}: How would you go about verifying the calibration results
in a quantitative manner? \\
\textbf{A}: There are many ways to verify the quanlity of the
calibration results. An empirical way of verifying the error is to see
if straight lines (in reality) appear straight in the image. This can
also be verified quantitatively by selecting points on the same line
and fitting a straight line by least-squares. The least-squares error
will also serve as an estimate of calibration error. \\ Another way is
to sample some points whose Cartesian co-ordinates (relative to the
camera lens) are known. Then, their pixel co-ordinates are estimated
by using the Camera Matrix. By comparing estimated pixel positions
with the real positions in the picture, the quality of calibration can
be judged. To make the comparison quantitative, the sum of squared
errors or the sum of absolute errors is often used. The smaller the
errors, better the calibration.

\section*{Task 4} 

In the task 4, depth is measured and the effect of changing a couple
of parameters is observed. The camera is placed parallel to the floor
in the front of a flat wall. A small window in the middle of depth
image is chosen. Provided window is small enough, the depth image
should have the same depth value at every pixel. The estimate of range
is the mean of all depths inside the window. The estimates are
compared to the ground truth distances, which are measured using a
scale. The error is the absolute value of the difference between
ground truth value and the estimates. The effect (on error) of
changing window size and distance to wall is observed. \\

As window size increases, there should be an increase in error. The
point on the wall closest to the centre of the lens is the point of
intersection of the principal axis and the plane of the wall. This
point should be at a distance equal to the ground truth value. All
other points on the wall will be greater in distance. Hence as we
widen the window, the error should increase. \\

As distance from the wall increases, the error is expected to
grow. The graph shows an increasing trend in the error with
distance. However, the variance is not strictly monotone increasing or
decreasing. Also, the variance is too small, of the order of
\(10^{-10}\ mm^2\).

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{NoFilterRange.jpg}
    \caption{Change in error with distance.}
    \label{fig:NoFilterRange}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{NoFilterRangeVariance.jpg}
    \caption{Change in variance with distance.}
    \label{fig:NoFilterRangeVariance}
\end{subfigure}
\caption{Change in error and variance with distance.} 
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{NoFilterWindowSize.jpg}
    \caption{Change in error with window size.}
    \label{fig:NoFilterWindowSize}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{NoFilterWindowSizeVariance.jpg}
    \caption{Change in variance with window size.}
    \label{fig:NoFilterWindowSizeVariance}
\end{subfigure}
\caption{Change in error and variance with window size.} 
\end{figure}

\section*{Task 5}

Apart from errors due to lens distortion there is often additional
noise in images. For example, Gaussian noise can be caused by poor
illumination, high temperature or can be generated in the electronic
circuits. The most important feature of Gaussian noise is that it is
independent of pixel and the signal intensity.  Another typical noise
is salt-and-pepper random noise. It appears as dark pixels in bright
regions or bright pixels in dark regions. This happens usually when
converting a analog quantity to digital. Shot noise, also called
Poisson noise, is typically caused by the variation in the number of
photons sensed at a given exposure level. These noises are independent
of each other at different pixels. However unlike Gaussian noise, Shot
noise is dependent on the intensity level.  \\ To improve the quality,
images are often post-processed to filter out noise. In this part of
the task, we will explain the effects of three commonly used filters:
Gaussian blur filter, median blur filter and bilateral filter. Each
one has its own properties and is used for particular purposes.  The
filters process images through kernels. Kernel is slid over the whole
image in a process called convolution of a filter kernel. The value of
the pixel at the centre of the kernel is updated by using the
surrounding pixels in the kernel.\\

The filters are named depending on how the surrounding pixels affect
the value of the centre pixel. In the median filter, the centre pixel
is assigned the median value of the kernel. The Gaussian blur
convolves the image with a 2-D Gaussian function. Thus the value of
the centre pixel is affected more by pixels closer to it than by
pixels farther away. The bilateral filter considers not only the
position relative to the centre pixel but also considers radiometric
differences (color intensity or depth distance).

A target scene is show in figure \ref{fig:target_scene} (RGB) and
figure \ref{fig:target_scene_depth} (scaled, unfiltered depth image). Different
filters were applied to this target scene. Kernel sizes of 3, 13 and
31 were used for the Gaussian and Bilateral filters. Kernel sizes of 3
and 5 were used for the Median filter. The following figures show the
effect of these filters on the target scene.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.50]{TargetScene.png}
    \caption{RGB image of target scene.}
    \label{fig:target_scene}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.50]{depth_image.jpg}
    \caption{Unfiltered depth image of target scene.}
    \label{fig:target_scene_depth}
\end{figure}

The following figures show the effect of Filters on the depth
image.

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{gaussian_blur_3.jpg}
    \caption{Gaussian Blur Filter.}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{median_blur_3.jpg}
    \caption{Median Blur Filter.}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{bilateral_blur_3.jpg}
    \caption{Bilateral Filter.}
\end{subfigure}
\caption{Different filters with a kernel size of 3.} 
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{gaussian_blur_13.jpg}
    \caption{Gaussian Blur Filter. Kernel size = 13.}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{median_blur_5.jpg}
    \caption{Median Blur Filter. Kernel size = 5.}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{bilateral_blur_13.jpg}
    \caption{Bilateral Filter. Kernel size = 13.}
\end{subfigure}
\caption{Gaussian and Bilateral filters with a kernel size of 13. Median Blur
  filter with a kernel size of 5.} 
\end{figure}

\textbf{Note}: The Median filter for floating point values can be applied only for a
kernel size of 3 and 5.

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{gaussian_blur_31.jpg}
    \caption{Gaussian Blur Filter.}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{bilateral_blur_31.jpg}
    \caption{Bilateral Filter.}
\end{subfigure}
\caption{Different filters with a kernel size of 31.} 
\end{figure}

All the filters tend to smoothen the image and cancel-out the
errors. The Gaussian and Bilateral filters tend to blur the edges. For
small kernel sizes, the effect of the filter is not very
apparent. For a kernel size of 13, the filters produce a
smooth image. The positive effect is that noise is cancelled-out to an
extent. However, for larger kernel sizes, the images tend to become
very blurry and there is information loss.\\

Averaging a few images is another way of filtering images (see
fig. \ref{MeanImage} and \ref{MedianImage}). Such a
filter can easily filter random noise. However, comparing the images,
it is easy to see that there is a lot of noise near the computer
display on the far right in figure \ref{MeanImage}. There is also
considerable noise on the near table. Thus, the blurring filters
produce a less noisy and smoother image of the scene. The averaging
filters produce a sharper image compared to the blurring filters.

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{average_image.jpg}
    \caption{Mean of ten images.}
    \label{MeanImage}
\end{subfigure} 
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{median_image.jpg}
    \caption{Median of ten images.}
    \label{MedianImage}
\end{subfigure}
\caption{Average of 10 images.} 
\end{figure}

\subsection*{Range measurement with filtering}
The above filtering techniques were applied to the window extracted
from the depth image. The Kernel size was chosen to be 31 in the case
of Gaussian and Bilateral filters. For Median filter, the kernel size
was chosen to be 5. The graphs below show the error vs ground truth
and error vs window size. It is quite clear that the filter has little
or no effect on the error. The graph of error vs window size after
Gaussian filter looks similar to that without the filter. However, the
graph for median filter looks anomalous.\\

\begin{table}[h]
\begin{center}
\begin{tabular}{c}
\begin{tabular}{c}
Effect of filtering in average error vs ground truth.
\end{tabular}\\
%\centering
\begin{tabular}{c c}
\includegraphics[scale=0.40]{GaussianRange.jpg} &
\includegraphics[scale=0.40]{MedianlRange.jpg} \\
Gaussian Filter & Median Filter
\end{tabular}\\
\begin{tabular}{c c}
\includegraphics[scale=0.40]{BilateralRange.jpg} &
\includegraphics[scale=0.40]{NoFilterRange.jpg} \\
Bilateral Filter & Without a filter
\end{tabular}
\end{tabular}
\end{center}
\end{table}


\begin{table}[h]
\begin{center}
\begin{tabular}{c}
\begin{tabular}{c}
Effect of filtering in average error vs window size.
\end{tabular}\\
%\centering
\begin{tabular}{c c}
\includegraphics[scale=0.40]{GaussianWindowSize.jpg} &
\includegraphics[scale=0.40]{MedianWindowSize.jpg} \\
Gaussian Filter & Median Filter
\end{tabular}\\
\end{tabular}
\end{center}
\end{table}

\textbf{Note}: The following conclusions were made about the method
used to measure the accuracy and estimate distance errors:
\begin{enumerate}
\item The use of point cloud data would produce more accurate
results. When using point cloud data, one could extract just the z
co-ordinate (in the frame of the kinect/xtion) and use that as
range. This would be the true distance to the wall from the
kinect/xtion as measured by the various rays.
\item The measurement accuracy when using a tape measure is \(\pm
  1mm\). A more accurate measuring tool would improve the accuracy of
  the results obtained. Eg. A laser.
\end{enumerate}
\end{document}